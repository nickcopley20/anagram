**Anagram Solver**

Given a file with a list words, one per line and ordered by size ascending. Read the file line by line, group all words
same size together as a batch. Then calculate groups of anagrams in each batch.


**Assumptions**

- The file is ordered ascending in size
- A group can have no anagrams

**Key Design Decision/Architectural Decisions**

- A scanner is used to allow us to use a regex to pick out the next words without having to 
read the next word to see if its biggest and then deal with the next group. This therefore meets the desire to only load
  one group into memory at a time.
  
- Java - I decided to use Java for this application, Kotlin and JavaScript would be other
good options but Java gives the advantage of checked exceptions.
  
**Running the code**

Gradle was used as the dependency management tool and also allows us to easily 
run the application using

./gradlew run --args=<path>

e.g.
[anagram (main)]$ ./gradlew run --args=/Users/ncopley/workspace/anagram/app/src/test/resources/test.txt

> Task :app:run

> cat,cta,atc

> bat

> three



**Big O**

###### Time
Although we scan the file to batch the words then add the words to a map, this would scale linearly and therefore its 
O(n)

###### Space
I thought of a few ways to do this but a map of letters as a fingerprint allows words to be added to the values of the
map for that key. Using a HashMap we only need to store the number of letters and not carry out sorting.

O(n)

**Java Microbench Harness**


| Benchmark | Mode | Threads | Samples | Score | Score Error (99.9%) | Unit |
| --------- | ---- | -------- | --------- | ---- | ---------- | -------|
| anagram.BenchMarkAnagram.solve | avgt | 1 | 5 | 0.204990 | 0.064487 | ms/op |

**Future**

- Maybe if downstream systems could consume the output; streaming the files and processing them in streams would allow the work
to be parallelised and therefore allow the system to be scaled.
  
- In addition to streaming it could be broken down into smaller microservices to allow individual aspects to scale independently
  
- If I had time and this code was on the hotpath and required the performance I'd use a profiler with larger example files 
to test how the garbage collection behaved and if there are any bottlenecks.

- A linter in the build script would be needed too

- For production I'd also recommend running it through static code analysis

- Observability - metrics, monitoring and maybe tracing should be adding for any production build.

- A logger should be used