package anagram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BatchReaderTest {

    @Test
    public void shouldFindTheFirstBatch() throws URISyntaxException {
        // Given a valid file with words
        BatchReader batchReader = getReaderWithValidFile();

        // When
        List<String> batch = batchReader.nextBatch();

        // Then
        assertEquals(Arrays.asList("bat", "cat","cta","atc"), batch);
    }



    @Test
    public void shouldFindTheSecondBatch() throws URISyntaxException {
        // Given
        BatchReader batchReader = getReaderWithValidFile();

        // When - call next Batch twice.
        batchReader.nextBatch();
        List<String> batch = batchReader.nextBatch();

        // Then
        assertEquals(Collections.singletonList("three"), batch);
    }

    @Test
    public void shouldThrowExceptionOnMissingFile()  {
        // Given
        File fileThatDoesntExist = new File("/tmp/doesntExist");

        // Then assert the FileNotFoundException is caught and a friendly IllegalArgumentException is thrown.
        Assertions.assertThrows(IllegalArgumentException.class,() -> {
            // When - construct the reader
            new BatchReader(fileThatDoesntExist);
        });
    }

    @Test
    public void shouldHandleEmptyFile() throws URISyntaxException {
            // Given an empty file with words
            URL resource = BatchReaderTest.class.getResource("/empty.txt");
            File file = Paths.get(resource.toURI()).toFile();
            BatchReader batchReader =  new BatchReader(file);

            // When
            List<String> batch = batchReader.nextBatch();

            // Then
            assertTrue(batch.isEmpty());
        }

    @Test
    public void shouldHandleNumericFile() throws URISyntaxException {
        // Given an empty file with words
        URL resource = BatchReaderTest.class.getResource("/numeric.txt");
        File file = Paths.get(resource.toURI()).toFile();
        BatchReader batchReader =  new BatchReader(file);

        // When
        List<String> batch = batchReader.nextBatch();

        // Then
        assertEquals(Arrays.asList("12", "21"), batch);
    }

    @Test
    public void shouldHandleNewLinesFile() throws URISyntaxException {
        // Given an empty file with words
        URL resource = BatchReaderTest.class.getResource("/emptyWithNewLines.txt");
        File file = Paths.get(resource.toURI()).toFile();
        BatchReader batchReader =  new BatchReader(file);

        // When
        List<String> batch = batchReader.nextBatch();

        // Then
        assertTrue(batch.isEmpty());
    }

    @Test
    public void shouldHandleNewLinesInValidFile() throws URISyntaxException {
        // Given an empty file with words
        URL resource = BatchReaderTest.class.getResource("/validWithNewLines.txt");
        File file = Paths.get(resource.toURI()).toFile();
        BatchReader batchReader =  new BatchReader(file);

        // When
        List<String> batch = batchReader.nextBatch();

        // Then
        assertEquals(Arrays.asList("bat", "cat","cta","atc"), batch);
    }



    @Test
    @Disabled("The Regex also parses word chars")
    public void shouldHandleSpecialCharFile() throws URISyntaxException {
        // Given an empty file with words
        URL resource = BatchReaderTest.class.getResource("/special.txt");
        File file = Paths.get(resource.toURI()).toFile();
        BatchReader batchReader =  new BatchReader(file);

        // When
        List<String> batch = batchReader.nextBatch();

        // Then
        assertEquals(Arrays.asList("12", "21"), batch);
    }


    private BatchReader getReaderWithValidFile() throws URISyntaxException {
        URL resource = BatchReaderTest.class.getResource("/test.txt");
        File file = Paths.get(resource.toURI()).toFile();
        return new BatchReader(file);
    }
}
