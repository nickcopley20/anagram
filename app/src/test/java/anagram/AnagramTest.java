package anagram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class AnagramTest {

    @Test
    public void shouldSolveAnagrams() throws URISyntaxException {
        Anagram anagram = new Anagram();
        URL resource = AnagramTest.class.getResource("/test.txt");
        File file = Paths.get(resource.toURI()).toFile();
        Assertions.assertDoesNotThrow(() -> anagram.solve(file));
    }

    @Test
    public void shouldFindAnagrams()
    {
        // Given a list of words of the same size.
        List<String> words = Arrays.asList("cat", "tac","bat", "cta");
        Anagram anagram = new Anagram();

        // When
        List<List<String>> groups = anagram.findAnagrams(words);

        // Then
        Assertions.assertEquals(2,groups.size());
        Assertions.assertEquals( Arrays.asList("cat", "tac", "cta"), groups.get(0));
        Assertions.assertEquals( Arrays.asList("bat"), groups.get(1));
    }

    @Test
    public void shouldFindAnagramsWithNumbers()
    {
        // Given a list of words of the same size.
        List<String> words = Arrays.asList("1234", "4321","5674", "5647");
        Anagram anagram = new Anagram();

        // When
        List<List<String>> groups = anagram.findAnagrams(words);

        // Then
        Assertions.assertEquals(2,groups.size());
        Assertions.assertEquals( Arrays.asList("5674", "5647"), groups.get(0));
        Assertions.assertEquals( Arrays.asList("1234", "4321"), groups.get(1));
    }

    @Test
    public void shouldFindAnagramsWithSpecialChars()
    {
        // Given a list of words of the same size.
        List<String> words = Arrays.asList("#~*%","#~%*",":$$");
        Anagram anagram = new Anagram();

        // When
        List<List<String>> groups = anagram.findAnagrams(words);

        // Then
        Assertions.assertEquals(2,groups.size());
        Assertions.assertEquals( Arrays.asList("#~*%","#~%*"), groups.get(0));
    }

    @Test
    public void shouldntFindAnagramsWithEmptyList()
    {
        // Given a list of words of the same size.
        List<String> words = Collections.emptyList();
        Anagram anagram = new Anagram();

        // When
        List<List<String>> groups = anagram.findAnagrams(words);

        // Then
        Assertions.assertEquals(0,groups.size());
    }


}
