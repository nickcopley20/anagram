package anagram;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * This class reads the file line by line grouping lines of the same size into batches.
 */
public class BatchReader {
    private final Scanner scanner;

    /**
     * Create a batch reader with the file to read line by line.
     *
     * @param file
     */
    public BatchReader(File file) {
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Please specify a path to a valid file");
        }
    }

    /**
     * Get the next batch of words of the same size.
     *
     * @return the batch of words
     */
    public List<String> nextBatch() {
        List<String> words = new ArrayList<>();
        // The assumption is that its order by size
        if (scanner.hasNext()) {
            // Get the first word of that length,
            String word = scanner.next();
            words.add(word);
            int length = word.length();

            String template = "\\w{0,%s}";
            String pattern = String.format(template, length);

            // Now add all words that still match the same length
            while (scanner.hasNext(pattern)) {
                words.add(scanner.next(pattern));
            }

        }

        return words;
    }

}
